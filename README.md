# GenshinData
Repository containing the data for the live version of Genshin Impact. This repository is a work between [FZFalzar](https://github.com/FZFalzar) and I.

---

If your project uses the data here it would be very much appreciated crediting or a thank you in it!